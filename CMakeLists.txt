cmake_minimum_required(VERSION 2.8.3)
project(robocupathome_pnp)

find_package(catkin REQUIRED COMPONENTS
  message_generation
  roscpp
  rospy
  std_msgs
  geometry_msgs
  nav_msgs
  pnp_ros
  tf
)

find_package(naoqi_bridge_msgs QUIET)
if(NOT naoqi_bridge_msgs_FOUND)
  message(WARNING "Couldn't find naoqi_bridge_msgs. Naoqi support disabled.")
else()
  message(STATUS "Found naoqi_bridge_msgs. Using Naoqi features.")
  SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} -DUSENAOQI") 
endif()

catkin_package(
   CATKIN_DEPENDS message_runtime
)

include_directories(
  ${pnp_ros_INCLUDE_DIRS}
  ${catkin_INCLUDE_DIRS}
)

add_executable(rcathome_pnpas src/main.cpp src/RCHPNPAS.cpp src/Actions.cpp src/ActionsBase.cpp src/Conditions.cpp src/TCPClient.cpp)

target_link_libraries(rcathome_pnpas ${pnp_ros_LIBRARIES} ${catkin_LIBRARIES})

